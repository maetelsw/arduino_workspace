#include <SPI.h>
byte count;
bool process_it;
byte receive;

void setup (void)
{
  Serial.begin(9600);
  // SPI 통신을 위한 핀들의 입출력 설정
  pinMode(MISO, OUTPUT);
  pinMode(MOSI, INPUT);
  pinMode(SCK, INPUT);
  pinMode(SS, INPUT);
  
  // 마스터의 전송 속도에 맞추어 통신 속도를 설정한다.
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  
  // SPI 통신을 위한 레지스터를 설정
  SPCR |= _BV(SPE); // SPI 활성화
  SPCR &= ~_BV(MSTR); // Slave 모드 선택
  SPCR |= _BV(SPIE); // 인터럽트 허용
  
  count = 'a'; // 카운터 초기화

  process_it = false;
  receive = 0;
}

// SPI 통신으로 데이터가 수신될 때 발생하는 인터럽트 처리 루틴
ISR (SPI_STC_vect)
{
  receive = SPDR;  
  SPDR = count;  // 카운터 값을 ASCII 값으로 전달
  process_it = true;
}

void loop (void)
{
  //count = (count + 1) ; // 카운터 값 증가

  if (process_it == true) {
    Serial.println(receive);
    process_it = false;    
  }

  delay(1000);
}
