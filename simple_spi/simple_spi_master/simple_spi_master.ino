#include <MAX11300.h>
#include <MAX11300Hex.h>

/* simple SPI Master
   The circuit:
   - /CNVT : D7
   - SS: D10
   - MOSI: D11
   - MISO: D12
   - SCK: D13
*/

#include <SPI.h>

void setup() {
  // initialize SPI
  SPI.begin();
  digitalWrite(SS, HIGH);  // de-selected
  digitalWrite(7, HIGH);  // de-selected
  Serial.begin(115200);
}

void loop() {
  // Device ID
  digitalWrite(SS, LOW);  // selected to 'Slave'
  delay(20);
  unsigned int received1 = SPI.transfer(0x01);  // Read Addr.'

  delay(20);
  unsigned int received2 = SPI.transfer(0x00);  // Skip for get data
  delay(20);
  unsigned int received3 = SPI.transfer(0x00);  // Skip for get data
  delay(20);

  delay(5);
  digitalWrite(SS, HIGH);
  Serial.println(received1, HEX);
  Serial.println(received2, HEX);
  Serial.println(received3, HEX);

  delay(200);

}
