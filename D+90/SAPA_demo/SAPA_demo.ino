/**
 * Copyright 2016, Jees Kim
 * 
 * 2016 HAX-9 D+90
   For the test sensor of mat on Arduino Uno

   Peripherals :
   BT module HC-05 - Serial1 6(Rx), 7(Tx)
   SPI interface with MAX11300 - 11(MISO), 12(MOSI), 13(SCK), 10(SS), 9(/CNVT)
*/

#include "MAX11300.h"
#include "SoftwareSerial.h"

#define MAX_CHANNEL_NUM 20

MAX11300 max11300;
SoftwareSerial BTSerial(2, 3); // Rx,Tx
uint16_t sensor_value_ch[MAX_CHANNEL_NUM];

void setup() {
  // for debugging to PC monitor
  Serial.begin(9600);

  // for BT : 38400-AT mode, 9600-normal mode
  BTSerial.begin(9600);

  // init SPI with MAX11300  
  max11300.begin(11, 12, 13, 10, 7); // void MAX11300::begin(uint8_t mosi, uint8_t miso, uint8_t sclk, uint8_t cs, uint8_t cnvt)
  
  // initialalize ADC value
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sensor_value_ch[i] = 0;
  }
}

void loop() {
  char tx_message[30], temp_str[20];

  // read the analog in value:
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    max11300.single_ended_adc_read(i, sensor_value_ch[i]);
  }

  // encode to '(prefix) + (each channel data)' over BT
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sprintf(tx_message, "$%02d:%04d", i, sensor_value_ch[i]);    
    Serial.println(tx_message);
    BTSerial.println(tx_message);
  }

  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(50);
}
