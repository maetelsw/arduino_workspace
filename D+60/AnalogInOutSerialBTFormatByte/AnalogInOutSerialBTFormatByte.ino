/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to 255
  and uses the result to set the pulsewidth modulation (PWM) of an output pin.
  Also prints the results to the serial monitor.

  The circuit:
   potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
   LED connected from digital pin 9 to ground

  created 29 Dec. 2008
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

*/

#include <SoftwareSerial.h>

#define MAX_CHANNEL_NUM 12

SoftwareSerial BTSerial(10, 11); // RX | TX

uint16_t sensor_value_ch[MAX_CHANNEL_NUM];


void setup() {
  // initialize bluetooth : 38400-AT mode, 9600-normal mode
  BTSerial.begin(9600);

  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  // initialize analog sensor value
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sensor_value_ch[i] = 0;
  }
}

void loop() {
  char tx_message[30], temp_str[20];

  // read the analog in value:
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sensor_value_ch[i] = analogRead(i);
  }

  // encode to '(prefix) + (each channel data)' over BT
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sprintf(tx_message, "$%02d:%04d", i, sensor_value_ch[i]);
    Serial.println(tx_message);
    BTSerial.println(tx_message);
  }

  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(50);

}
