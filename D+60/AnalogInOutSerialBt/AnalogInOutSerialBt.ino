/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to 255
  and uses the result to set the pulsewidth modulation (PWM) of an output pin.
  Also prints the results to the serial monitor.

  The circuit:
   potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
   LED connected from digital pin 9 to ground

  created 29 Dec. 2008
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

*/
#include <SoftwareSerial.h>

#define MAX_CHANNEL_NUM 12
#define ADC_SAMPLE_BUFFER 24  // channel * 2

SoftwareSerial BTSerial(10, 11); // RX | TX

uint16_t sensor_value_ch[MAX_CHANNEL_NUM];
uint16_t ch[MAX_CHANNEL_NUM];
char send_to_bytes[100];
char test_byte[10];



void setup() {
  // initialize bluetooth : 38400-AT mode, 9600-normal mode
  BTSerial.begin(9600);

  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  // initialize analog sensor value
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sensor_value_ch[i] = 0;
  }

  for (int i = 0; i < 10; i++) {
    test_byte[i] = i;
  }
}

void loop() {
  BTSerial.println("123456");
  Serial.println(sizeof(test_byte));

  // read the analog in value:
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    sensor_value_ch[i] = analogRead(i);
  }

  // map it to the range of the analog out:
  for (int i = 0; i < MAX_CHANNEL_NUM; i++) {
    ch[i] = map(sensor_value_ch[i], 0, 1023, 0, 4095);
    //Serial.println(ch[i]);

    if (sizeof(ch[i]) != 2) {
      Serial.println(sizeof(ch[i]));
    }
  }

  // encode data to send bluetooth, casting interger to byte array
  /*for (int i = 0; i < 8; i++) {
    send_to_bytes[i * 2] = ch[i];
    send_to_bytes[(i * 2) + 1] = ch[i] >> 8;
    }*/
  send_to_bytes[10] = NULL;

  //BTSerial.println(send_to_bytes);

  //sprintf(send_to_bytes, "%c%c%c%c%c%c%c%c%c%c%c%c", ch[0], ch[1], ch[2], ch[3], ch[4], ch[5], ch[6], ch[7], ch[8], ch[9], ch[10], ch[11]);
  //sprintf(send_to_bytes, "%c%c%c%c%c%c%c%c%c%c%c%c", 1,2,3,4,5,6,7,8,9,10,11,12);

  //BTSerial.println("123456789012345678901234567890");
  //Serial.println(sizeof(send_to_bytes));

  // send value over bluetooth
  // print the results to the serial monitor:
  //BTSerial.println(send_to_bytes);

  /*for (int i = 0; i < sizeof(value); i++) {
    sprintf(string, "%02d=%d", i, value[i]);
    Serial.println(string);
    }*/

  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(200);

}
