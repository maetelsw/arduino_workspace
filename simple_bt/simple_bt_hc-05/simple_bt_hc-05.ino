#include <SoftwareSerial.h>

SoftwareSerial BTSerial(10, 11); // RX | TX
int count = 0;

void setup()
{
  Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BTSerial.begin(9600);  // HC-05 default speed in AT command more
}
void loop()
{
  //Read from bluetooth and write to usb serial
  if(BTSerial.available())
  {
    char toSend = (char)BTSerial.read();
    Serial.print(toSend);
  }

  //Read from usb serial to bluetooth
  if(Serial.available())
  {
    char toSend = (char)Serial.read();
    BTSerial.print(toSend);
  }
}
